import 'package:flutter/material.dart';
import 'dart:math';

import 'image_widget.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Clicker App',
      theme: ThemeData(
        primaryColor: Colors.redAccent[400],

      ),
      home: MyHomePage(title: 'Flutter Clicker App Vol. 3'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _rNumber = 0;
  String plusMinus = 'Press Me. ';
  int _swapper = 0;

  void _randomizer(){
    setState(() {
      var rng = new Random();
      _rNumber = rng.nextInt(100);
    });
  }

  void _incrementor(){
    if(_swapper == 0){
      _counter += _rNumber;
      _swapper++;
      plusMinus = '+';
    } else if(_swapper == 1){
      _counter -= _rNumber;
      _swapper--;
      plusMinus = '-';

    } else{
      print('Error');
    }
  }

  void _incrementCounter() {
    setState(() {
      _counter++;
    });
  }

  void _incrementDeCounter() {
    setState(() {
      _counter--;
    });
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(

        title: Text(widget.title),
      ),
      body: ListView(
        scrollDirection: Axis.vertical,
        children: <Widget>[
        CusWidget(),
        Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
              width: 380,
              height: 100,
              margin: EdgeInsets.all(15),
              decoration: BoxDecoration(
                color: Colors.redAccent[200].withOpacity(0.9),
                borderRadius: BorderRadius.circular(15),
                boxShadow: [
                  BoxShadow(
                    color: Colors.redAccent[400],
                    blurRadius: 1.0, // softening the shadow
                    spreadRadius: 1.0, // extending the shadow
                    offset: Offset(
                      4.0, // horizontal
                      3.0, // vertical
                    ),
                  )
                ],
              ),
              child: InkWell(
                onTap: (){
                  _randomizer();
                  _incrementor();
                },
                child: Center(
                  child: Text(
                    plusMinus+_rNumber.toString(),
                    style: TextStyle(
                        fontSize: 40,
                        fontWeight: FontWeight.bold,
                        color: Colors.white
                    ),
                  ),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(top: 50.0, bottom: 25.0),
              child: Text(
                'You have pushed the button this many times:',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
            ),
            Text(
              '$_counter',
              style: TextStyle(fontSize: 40, fontWeight: FontWeight.bold),
              //style: Theme.of(context).textTheme.headline4,
            ),
            Container(
              padding: EdgeInsets.only(top: 60, bottom: 40),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Container(
                    height: 150,
                    width: 150,
                    decoration: BoxDecoration(
                      color: Colors.greenAccent[400],
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.green,
                          blurRadius: 10.0, // softening the shadow
                          spreadRadius: 0.0, // extending the shadow
                          offset: Offset(
                            4.0, // horizontal
                            3.0, // vertical
                          ),
                        )
                      ],
                    ),
                    child: InkWell(
                      child: Center(
                        child: Text("+",
                            style: TextStyle(fontSize: 50, color: Colors.white)
                        ),
                      ),
                      onTap: (){
                        _incrementCounter();
                      },
                    ),
                  ),
                  Container(
                    height: 150,
                    width: 150,
                    decoration: BoxDecoration(
                      color: Colors.redAccent[400],
                      borderRadius: BorderRadius.circular(15),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.red,
                          blurRadius: 10.0, // softening the shadow
                          spreadRadius: 0.0, // extending the shadow
                          offset: Offset(
                            4.0, // horizontal
                            3.0, // vertical
                          ),
                        )
                      ],
                    ),
                    child: InkWell(
                      child: Center(
                        child: Text("-",
                            style: TextStyle(fontSize: 50, color: Colors.white)
                        ),
                      ),
                      onTap: (){
                        _incrementDeCounter();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ],),
    );
  }
}