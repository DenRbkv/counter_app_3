import 'package:flutter/material.dart';

import 'variables.dart';

class CusWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Container(
        height: 400,
        color: Colors.blue,
        child: Center(
          child: Container(
            height: 350,
            width: 350,
            child: Image.network(flutterImage),
          ),
        ),
      ),
      Container(
        height: 70,
        color: Colors.lightBlueAccent,
        child: Center(
          child: Text('Yep, this is Flutter',
          style: imgStyle,
          ),
        ),
      ),
      Container(
        height: 400,
        color: Colors.grey,
        child: Center(
          child: Container(
            height: 350,
            width: 350,
            child: Image.network(vestoImage),
          ),
        ),
      ),
      Container(
        height: 70,
        color: Colors.blueGrey,
        child: Center(
          child: Text('The AppVesto logo',
            style: imgStyle,
          ),
        ),
      ),
      ],);
  }
}